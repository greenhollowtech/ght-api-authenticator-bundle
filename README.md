GHT API Authenticator Bundle
=========================

This bundle provides the [GHT API Authenticator](https://bitbucket.org/greenhollowtech/ght-api-authenticator) as a service in Symfony.

# Installation

## Get the Composer package

To install with [Composer](https://getcomposer.org/), run `composer require greenhollowtech/ght-api-authenticator-bundle`.  For Symfony 2, make sure to stipulate the version `~1.0`.

## Add the GHTApiAuthenticatorBundle to your Symfony application

```php
// app/AppKernel.php

    public function registerBundles()
    {
        return array(
            // ...
            new GHT\ApiAuthenticatorBundle\GHTApiAuthenticatorBundle(),
            // ...
        );
    }
```

# Usage

Set up your firewall to use the API Authenticator.

```yml
# app/config/security.yml

security:
    ...
    firewalls:
        api:
            stateless: true
            simple_preauth:
                authenticator: ght_api_authenticator
```

Add the `apiKey` and `apiSecret` properties to your user entity, along with the related getters and setters.

If you already have properties established, and wish to use the existing data, simply configure the API Authenticator to use those properties instead of the default:

```yml
# app/config/config.yml

ght_api_authenticator:
    user_entity:
        api_key_property: myKeyPropertyName
        api_secret_property: mySecretPropertyName
```

# Custom User Provider

If your application has alternative means of loading the current user, you can provide that to the API Authenticator as well by defining your own service.  Your custom user provider is passed as the third argument in the construct.

```yml
# src/AcmeBundle/Resources/config/services.yml

services:
    ...
    acme_api_user_authenticator:
        class: GHT\ApiAuthenticatorBundle\Security\ApiUserAuthenticator
        arguments:
            - %ght_api_authenticator.user_entity.api_key_property%
            - %ght_api_authenticator.user_entity.api_secret_property%
            - "@acme_custom_user_provider"
```

Then set the authenticator in your firewall to use this new service instead.
