<?php

namespace GHT\ApiAuthenticatorBundle\Tests\Security;

use GHT\ApiAuthenticatorBundle\Security\ApiUserAuthenticator;
use GHT\ApiAuthenticatorBundle\Fixtures\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\PreAuthenticatedToken;

/**
 * Exercises the ApiUserAuthenticator.
 */
class ApiUserAuthenticatorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown()
    {
    }

    /**
     * Create a Request.
     *
     * @param string $apiKey The API key passed in the request credentials.
     * @param string $apiSecret The API secret used to hash the request credentials.
     *
     * @return Symfony\Component\HttpFoundation\Request
     */
    protected function configureRequest($apiKey, $apiSecret)
    {
        $postData = array(
            'data' => 'Some test data',
        );

        // Compile the hash data as it would be by the GHT\PhpApi\ApiClient
        $hashData = array_merge($postData, array(
            'api-key' => $apiKey,
            'api-method' => 'POST',
            'api-url' => 'http://test.greenhollowtech.com/api/test',
        ));

        ksort($hashData);

        $authorizationHeader = sprintf(
            'user=%s,key=%s,hash=%s',
            'testUser',
            $apiKey,
            hash_hmac('sha256', json_encode($hashData), $apiSecret)
        );

        // Configure the Request
        $request = new Request(
            array(), // $_GET
            $postData, // $_POST
            array(), // attributes
            array(), // $_COOKIE
            array(), // $_FILES
            array( // $_SERVER
                'HTTP_AUTHORIZATION' => $authorizationHeader,
                'REQUEST_URI' => '/api/test',
                'SERVER_NAME' => 'test.greenhollowtech.com',
                'SERVER_PORT' => 80,
            )
        );
        $request->setMethod('POST');

        return $request;
    }

    /**
     * Mock the user provider.
     *
     * @param string $apiKey The API key for the user.
     * @param string $apiSecret The API secret for the user.
     *
     * @return Symfony\Component\Security\Core\User\InMemoryUserProvider
     */
    protected function configureUserProvider($apiKey, $apiSecret)
    {
        // The user entity provided by the implementation is expected to have
        // the additional getters and setters for the API key and secret
        $user = new User();
        $user->setApiKey($apiKey);
        $user->setApiSecret($apiSecret);

        $userProvider = $this->getMockBuilder('Symfony\Component\Security\Core\User\InMemoryUserProvider')
            ->disableOriginalConstructor()
            ->setMethods(array('loadUserByUsername'))
            ->getMock()
        ;

        $userProvider->expects($this->once())
            ->method('loadUserByUsername')
            ->will($this->returnValue($user))
        ;

        return $userProvider;
    }

    /**
     * Verify that a token is created as expected.
     */
    public function testCreateToken()
    {
        $request = $this->configureRequest('testKey', 'testSecret');

        $authenticator = new ApiUserAuthenticator('apiKey', 'apiSecret');
        $token = $authenticator->createToken($request, 'testProviderKey');

        $this->assertTrue($token instanceof PreAuthenticatedToken);
    }

    /**
     * Verify that an exception is thrown when attempting to create a token
     * with missing authorization credentials.
     */
    public function testCreateTokenBadCredentials()
    {
        $request = $this->configureRequest('', 'testSecret');

        $authenticator = new ApiUserAuthenticator('apiKey', 'apiSecret');

        $this->setExpectedException(
            'Symfony\Component\Security\Core\Exception\AuthenticationException',
            'Authorization header missing key value.'
        );

        // Trigger the exception
        $token = $authenticator->createToken($request, 'testProviderKey');
    }

    /**
     * Verify that the token created is determined as a supported type.
     */
    public function testSupportsToken()
    {
        $request = $this->configureRequest('testKey', 'testSecret');

        $authenticator = new ApiUserAuthenticator('apiKey', 'apiSecret');
        $token = $authenticator->createToken($request, 'testProviderKey');

        $this->assertTrue($authenticator->supportsToken($token, 'testProviderKey'));
    }

    /**
     * Verify that the token created is determined as not a supported type when
     * the provider key is mismatched.
     */
    public function testSupportsTokenMismatchedProviderKey()
    {
        $request = $this->configureRequest('testKey', 'testSecret');

        $authenticator = new ApiUserAuthenticator('apiKey', 'apiSecret');
        $token = $authenticator->createToken($request, 'testProviderKey');

        $this->assertFalse($authenticator->supportsToken($token, 'badProviderKey'));
    }

    /**
     * Verify that a token can be authenticated as expected.
     */
    public function testAuthenticateToken()
    {
        $request = $this->configureRequest('testKey', 'testSecret');
        $userProvider = $this->configureUserProvider('testKey', 'testSecret');

        $authenticator = new ApiUserAuthenticator('apiKey', 'apiSecret');
        $token = $authenticator->createToken($request, 'testProviderKey');

        // Trigger the expectations, no exceptions thrown
        $authenticator->authenticateToken($token, $userProvider, 'testProviderKey');
    }

    /**
     * Verify that an exception is thrown when a token is attempted to be
     * authenticated with bad credentials.
     */
    public function testAuthenticateTokenBadCredentials()
    {
        $request = $this->configureRequest('testKey', 'hackedSecret');
        $userProvider = $this->configureUserProvider('testKey', 'testSecret');

        $authenticator = new ApiUserAuthenticator('apiKey', 'apiSecret');
        $token = $authenticator->createToken($request, 'testProviderKey');

        $this->setExpectedExceptionRegExp(
            'Symfony\Component\Security\Core\Exception\AuthenticationException',
            '/^API hash is invalid for data:.*/'
        );

        // Trigger the exception
        $authenticator->authenticateToken($token, $userProvider, 'testProviderKey');
    }

    /**
     * Verify that an exception is thrown when a token is attempted to be
     * authenticated and the configured entity property is the wrong one.
     */
    public function testAuthenticateTokenWrongProperty()
    {
        $request = $this->configureRequest('testKey', 'testSecret');
        $userProvider = $this->configureUserProvider('testKey', 'testSecret');

        $authenticator = new ApiUserAuthenticator('apiKey', 'password');
        $token = $authenticator->createToken($request, 'testProviderKey');

        $this->setExpectedExceptionRegExp(
            'Symfony\Component\Security\Core\Exception\AuthenticationException',
            '/^API hash is invalid for data:.*/'
        );

        // Trigger the exception
        $authenticator->authenticateToken($token, $userProvider, 'testProviderKey');
    }

    /**
     * Verify that an exception is thrown when a token is attempted to be
     * authenticated and the configured entity property doesn't exist.
     */
    public function testAuthenticateTokenBadConfiguration()
    {
        $request = $this->configureRequest('testKey', 'testSecret');
        $userProvider = $this->configureUserProvider('testKey', 'testSecret');

        $authenticator = new ApiUserAuthenticator('doesNotExist', 'apiSecret');
        $token = $authenticator->createToken($request, 'testProviderKey');

        $this->setExpectedException(
            'Symfony\Component\DependencyInjection\Exception\InvalidArgumentException',
            'Configured property "doesNotExist" not set in GHT\ApiAuthenticatorBundle\Fixtures\User'
        );

        // Trigger the exception
        $authenticator->authenticateToken($token, $userProvider, 'testProviderKey');
    }
}
