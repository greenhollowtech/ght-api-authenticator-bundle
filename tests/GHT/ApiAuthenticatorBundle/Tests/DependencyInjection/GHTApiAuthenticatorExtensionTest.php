<?php

namespace GHT\ApiAuthenticatorBundle\Tests\DependencyInjection;

use GHT\ApiAuthenticatorBundle\DependencyInjection\GHTApiAuthenticatorExtension;
use Matthias\SymfonyDependencyInjectionTest\PhpUnit\AbstractExtensionTestCase;

/**
 * Exercises the dependency injection on a compiler pass of the
 * GHTApiAuthenticatorBundle.
 */
class GHTApiAuthenticatorExtensionTest extends AbstractExtensionTestCase
{
    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        parent::setUp();
    }

    /**
     * {@inheritdoc}
     */
    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * {@inheritdoc}
     */
    protected function getContainerExtensions()
    {
        return array(
            new GHTApiAuthenticatorExtension(),
        );
    }

    /**
     * Verify that default parameters have been set.
     */
    public function testParametersSetAfterLoadingDefaultValues()
    {
        $this->load();

        $this->assertContainerBuilderHasParameter('ght_api_authenticator.user_entity.api_key_property', 'apiKey');
    }

    /**
     * Verify that parameters have been set with custom values.
     */
    public function testParametersSetAfterLoadingWithCustomValue()
    {
        $this->load(array(
            'user_entity' => array(
                'api_key_property' => 'myCustomApiKey',
            )
        ));

        $this->assertContainerBuilderHasParameter('ght_api_authenticator.user_entity.api_key_property', 'myCustomApiKey');
    }
}