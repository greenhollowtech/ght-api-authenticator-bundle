<?php

namespace GHT\ApiAuthenticatorBundle\Security;

use GHT\ApiAuthenticator\GHTApiAuthenticator;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException as DependencyInjectionException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Http\Authentication\SimplePreAuthenticatorInterface;
use Symfony\Component\Security\Core\Authentication\Token\PreAuthenticatedToken;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * Authenticate API tokens for the user provider.
 */
class ApiUserAuthenticator implements SimplePreAuthenticatorInterface
{
    /**
     * @var string
     */
    protected $userKeyProperty;

    /**
     * @var Symfony\Component\Security\Core\User\UserProviderInterface
     */
    protected $userProvider;

    /**
     * @var string
     */
    protected $userSecretProperty;

    /**
     * The constructor.
     *
     * @param string $userKeyProperty The user entity's API key property name.
     * @param string $userSecretProperty The user entity's API secret property name.
     * @param Symfony\Component\Security\Core\User\UserProviderInterface $userProvider A custom UserProviderInterface.
     */
    public function __construct($userKeyProperty, $userSecretProperty, $userProvider = null)
    {
        $this->userKeyProperty = $userKeyProperty;
        $this->userProvider = $userProvider;
        $this->userSecretProperty = $userSecretProperty;
    }

    /**
     * Authenticate the token that contains the user credentials.  The default
     * UserProvider is used unless a custom one is set.
     *
     * @param Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token An instance of the PreAuthenticatedToken.
     * @param Symfony\Component\Security\Core\User\UserProviderInterface $userProvider User Provider.
     * @param string $providerKey The provider key.
     *
     * @return Symfony\Component\Security\Core\Authentication\Token\PreAuthenticatedToken
     */
    public function authenticateToken(TokenInterface $token, UserProviderInterface $userProvider, $providerKey)
    {
        $credentials = $token->getCredentials();
        $userProvider = ($this->userProvider instanceof UserProviderInterface) ? $this->userProvider : $userProvider;

        // Verify that the user exists
        $user = $userProvider->loadUserByUsername($credentials['api-user']);

        // Get the user's API credentials
        $getter = $this->getUserGetter($user, $this->userKeyProperty);
        $apiKey = $user->$getter();
        $getter = $this->getUserGetter($user, $this->userSecretProperty);
        $apiSecret = $user->$getter();

        // Validate the user's credentials (throws an exception if invalid)
        try {
            GHTApiAuthenticator::validate($apiKey, $apiSecret, $credentials);
        }
        catch (\Exception $e) {
            throw new AuthenticationException($e->getMessage());
        }

        return new PreAuthenticatedToken(
            $user,
            $credentials,
            $providerKey,
            $user->getRoles()
        );
    }

    /**
     * Create an anonymous token containing the credentials to be authenticated.
     *
     * @param Symfony\Component\HttpFoundation\Request $request The HTTP request.
     * @param string $providerKey The provider key.
     *
     * @return Symfony\Component\Security\Core\Authentication\Token\PreAuthenticatedToken
     */
    public function createToken(Request $request, $providerKey)
    {
        // Get the authentication parameters
        try {
            $credentials = GHTApiAuthenticator::getCredentials($request);
        }
        catch (\Exception $e) {
            throw new AuthenticationException($e->getMessage());
        }

        return new PreAuthenticatedToken(
            'anon.',
            $credentials,
            $providerKey
        );
    }

    /**
     * Get the getter method for a given property of the user entity.
     *
     * @param object $user The user entity.
     * @param string $property The property to be gotten.
     *
     * @return string
     */
    protected function getUserGetter($user, $property)
    {
        $getter = 'get' . strtoupper(substr($property, 0, 1)) . substr($property, 1);

        if (!method_exists($user, $getter)) {
            throw new DependencyInjectionException(sprintf(
                'Configured property "%s" not set in %s',
                $property,
                get_class($user)
            ));
        }

        return $getter;
    }

    /**
     * Checks whether this provider supports the given token.
     *
     * @param Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token An instance of the TokenInterface.
     * @param string $providerKey The provider key.
     *
     * @return boolean
     */
    public function supportsToken(TokenInterface $token, $providerKey)
    {
        return $token instanceof PreAuthenticatedToken && $token->getProviderKey() === $providerKey;
    }
}