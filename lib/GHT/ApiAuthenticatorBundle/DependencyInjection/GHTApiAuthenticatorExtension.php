<?php

namespace GHT\ApiAuthenticatorBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * Load the GHTApiAuthenticatorBundle configuration.
 */
class GHTApiAuthenticatorExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();

        $config = $this->processConfiguration($configuration, $configs);

        array_walk(
            $config,
            array($this, 'setParameters'),
            array('parentKey' => $this->getAlias(), 'container' => $container)
        );

        // Load services
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config/'));
        $loader->load('services.yml');
    }

    /**
     * {@inheritDoc}
     */
    public function getAlias()
    {
        return 'ght_api_authenticator';
    }

    /**
     * Recursively set all the configuration values as parameters.
     *
     * @param array|string $config
     * @param string $key
     * @param array $params
     */
    public function setParameters($config, $key, $params)
    {
        // Recursively set the parameter name
        $parameterName = $params['parentKey'] . '.' . $key;

        // If the configuration does not have children, set the parameter
        if (!is_array($config)) {
            $params['container']->setParameter($parameterName, $config);
            return;
        }

        // Otherwise, recursively call this function for each array value
        array_walk(
            $config,
            array($this, 'setParameters'),
            array('parentKey' => $parameterName, 'container' => $params['container'])
        );
    }
}
