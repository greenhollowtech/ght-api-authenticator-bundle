<?php

namespace GHT\ApiAuthenticatorBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Defines configuration settings.
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('ght_api_authenticator');

        $rootNode
            ->children()
                ->arrayNode('user_entity')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('api_key_property')->defaultValue('apiKey')->end()
                        ->scalarNode('api_secret_property')->defaultValue('apiSecret')->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
